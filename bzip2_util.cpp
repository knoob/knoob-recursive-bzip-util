#include "tinydir.h"

#include <string>
#include <vector>
#include <stack>
#include <iostream>

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cout << "Incorrect parameters.\nUsage:\n\'./bzip2_util <root_path>\'\n";
		return 0;
	}
	
	std::stack<std::string> d_stack;
	
	std::string base = argv[1];
	
	std::cout << "Starting bzipping process from: " << argv[1] << std::endl;
	std::cout << "This might take several minutes, depending on the size of your folder!" << std::endl;
	std::cout << "The Bzipping will run in the background until it\'s completed!" << std::endl;
	
	d_stack.push(argv[1]);
	
	while (!d_stack.empty()) {
		tinydir_dir d;
		
		tinydir_open_sorted(&d, d_stack.top().c_str());
		
		std::string top = d_stack.top();
		d_stack.pop();

		for (unsigned i = 0; i < d.n_files; ++i) {
			tinydir_file f;
			tinydir_readfile_n(&d, &f, i);
			
			std::string file = top + f.name;

			if (f.is_dir && f.name[0] != '.') {
				d_stack.push(file + "/");
			} else if (f.name[0] != '.') {
				if (strcmp(f.extension, "bz2")) {
					file = "bzip2 --force \"" + file + "\" &";
					system(file.c_str());
				}
			}
		}
	}
}

